package UPIS;

public class Horario {
		
	private byte hora;
	private byte minuto;
	private byte segundo;
		
	public Horario() {
		setHora((byte)0);
		setMinuto((byte)0);
		setSegundo((byte)0);
	}
	
	public void setHora(byte hora) {
		if(hora >= 0 && hora <= 23) {
			this.hora = hora;
		}
	}
	
	public void setMinuto(byte minuto) {
		if(minuto >= 0 && minuto <= 59) {
			this.minuto = minuto;
		}
	}
	
	public void setSegundo(byte segundo) {
		if(segundo >= 0 && segundo <= 59) {
			this.segundo = segundo;
		}
	}
	
	public byte getHora() {
		return hora;
	}
	
	public byte getMinuto() {
		return minuto;
	}
	
	public byte getSegundo() {
		return segundo;
	}
		
	public void incrementaSegundo() {
		
		byte s = (byte)(segundo + 1);
		
		if (s == 60) {
			incrementaMinuto();
			segundo = 0;
		}
		else {
			setSegundo((byte)(segundo + 1));
		}
	}
	
	public void incrementaMinuto() {
		
		byte m = (byte)(minuto + 1);
		
		if (m == 60) {
			incrementaHora();
			minuto = 0;
		}
		else {
			setMinuto((byte)(minuto + 1));
		}
	}
	
	public void incrementaHora() {
		
		byte h = (byte)(hora + 1);
		
		if (h == 24) {
			hora = 0;
		}
		else {
			setHora((byte)(hora + 1));
		}
	}
	
}
